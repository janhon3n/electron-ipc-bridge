const { contextBridge } = require('electron')
import { ipcRenderer } from "electron"
import { generateId } from "../shared/utils"
import { ElectronIpcBridgeAPI } from "./bridge-api-type"

const api: ElectronIpcBridgeAPI = {

    invokeMethod: async (target, ...args) => {
        const { serviceName, functionName, } = target
        const res = await ipcRenderer.invoke(`invoke-${serviceName}`, functionName, ...args)
        return res
    },

    subscibeToEventChannel: (target, nextHandler, errorHandler) => {
        const { channelName, serviceName } = target

        const subscriptionId = generateId()

        ipcRenderer.on(`subscription-next-${subscriptionId}`, (_, data) => nextHandler(data))
        ipcRenderer.on(`subscription-error-${subscriptionId}`, (_, error) => errorHandler(error))
        ipcRenderer.send(`subscribe-${serviceName}`, {
            subscriptionId: subscriptionId,
            channelName: channelName,
        })

        return {
            unsubscribe: () => {
                ipcRenderer.send(`unsubscribe-${serviceName}`, {
                    subscriptionId: subscriptionId,
                    channelName: channelName,
                })
                ipcRenderer.removeListener(`subscription-next-${subscriptionId}`, nextHandler)
                ipcRenderer.removeListener(`subscription-error-${subscriptionId}`, errorHandler)
            }
        }
    },

    sendEventChannelNext: (target, value) => {
        const { channelName, serviceName } = target

        ipcRenderer.send(`next-${serviceName}`, {
            channelName,
            value,
        })
    }
}


export function exposeApiInPreload() {
    contextBridge.exposeInMainWorld('electronIpcBridgeAPI', api)
}
