export interface ElectronIpcBridgeAPI {

    invokeMethod: <D = any>(
        target: {
            serviceName: string,
            functionName: string
        },
        ...args: any[]
    ) => Promise<D>

    subscibeToEventChannel: <E = any>(
        target: {
            serviceName: string
            channelName: string
        },
        nextHandler: (data: E) => void,
        errorHandler: (error: any) => void,
    ) => {
        unsubscribe: () => void
    }

    sendEventChannelNext: <E = any>(
        target: {
            serviceName: string
            channelName: string
        },
        value: E
    ) => void
}
