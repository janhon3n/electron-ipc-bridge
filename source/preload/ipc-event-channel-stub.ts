import { IpcEventChannel } from "../shared/ipc-event-channel"
import { ElectronIpcBridgeAPI } from "./bridge-api-type"

export class IpcEventChannelStub<E> extends IpcEventChannel<E> {

    private subscription: { unsubscribe: () => void } | null = null

    constructor(private target: {
        serviceName: string,
        channelName: string,
    }) {
        super()
        this.ipcNextHandler = this.ipcNextHandler.bind(this)
        this.ipcErrorHandler = this.ipcErrorHandler.bind(this)
    }

    public addListener(listenerId: string, listener: (data: E) => void) {
        if (this.subscription == null) {
            this.subscribeToIpc()
        }
        super.addListener(listenerId, listener)
    }

    public removeListener(listenerId: string) {
        super.removeListener(listenerId)
        if (this.listeners.size === 0 && this.subscription != null) {
            this.unsubscribeFromIpc()
        }
    }

    public next(value: E) {
        const api = (window as any).electronIpcBridgeAPI as ElectronIpcBridgeAPI
        api.sendEventChannelNext(this.target, value)
    }

    protected ipcNextHandler(value: E) {
        super.next(value)
    }

    protected ipcErrorHandler(error: string) {
        console.error("IpcEventEmitter error: " + error)
    }

    private subscribeToIpc() {
        const api = (window as any).electronIpcBridgeAPI as ElectronIpcBridgeAPI

        this.subscription = api.subscibeToEventChannel<E>(
            this.target,
            this.ipcNextHandler,
            this.ipcErrorHandler
        )
    }

    private unsubscribeFromIpc() {
        this.subscription?.unsubscribe()
        this.subscription = null
    }
}
