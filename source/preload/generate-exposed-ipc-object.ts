import { IIpcDeclaration } from "../types"
import { IpcEventChannelStub } from "./ipc-event-channel-stub"
import { ElectronIpcBridgeAPI } from "./bridge-api-type"


export function generateExposedIpcObject<T>(declaration: IIpcDeclaration) {

    const api = (window as any).electronIpcBridgeAPI as ElectronIpcBridgeAPI

    const object = {} as any

    for (const eventEmitter of declaration.eventChannels) {
        object[eventEmitter] = new IpcEventChannelStub({
            serviceName: declaration.name,
            channelName: eventEmitter,
        })
    }

    for (const method of declaration.methods) {
        object[method] = async (...args: any) => {
            const res = await api.invokeMethod({
                functionName: method,
                serviceName: declaration.name,
            }, ...args)

            if (res.error) throw res.error
            return res.data
        }
    }

    return object as T
}
