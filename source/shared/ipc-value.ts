import { IIpcValue } from "../types"
import { IpcEventChannel } from "./ipc-event-channel"

export class IpcValue<D> extends IpcEventChannel<D> implements IIpcValue<D> {

    private _value: D
    public get value() {
        return this._value
    }

    constructor(initialValue: D) {
        super()
        this._value = initialValue
    }

    public next(value: D) {
        this._value = value
        this.emit(value)
    }

    public addListener(listenerId: string, listener: (value: D) => void) {
        super.addListener(listenerId, listener)
        // Send current value immediately
        listener(this._value)
    }

    private emit(event: D) {
        for (const listener of this.listeners.values()) {
            listener(event)
        }
    }
}
