import { IIpcEventChannel } from ".."

export class IpcEventChannel<E> implements IIpcEventChannel<E> {
    protected listeners = new Map<string, (data: E) => void>()

    public addListener(listenerId: string, listener: (event: E) => void) {
        this.listeners.set(listenerId, listener)
    }

    public removeListener(listenerId: string) {
        this.listeners.delete(listenerId)
    }

    public next(event: E) {
        for (const listener of this.listeners.values()) {
            listener(event)
        }
    }
}

