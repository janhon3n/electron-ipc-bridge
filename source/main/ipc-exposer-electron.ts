import { ipcMain, WebContents } from "electron";
import { IIpcDeclaration } from "../types";
import { IpcExposer } from "./ipc-exposer";

export class IpcExposerElectron extends IpcExposer {
    constructor(args: {
        declaration: IIpcDeclaration
        webContents: WebContents,
        object: any,
    }) {
        super({
            declaration: args.declaration,
            object: args.object,

            receiver: {
                // remove electron events from traffic

                on: (channel, listener) => {
                    ipcMain.on(channel, (event, ...args) => {
                        listener(...args)
                    })
                },

                handle: (channel, handler) => {
                    ipcMain.handle(channel, (event, ...args) => {
                        return handler(...args)
                    })
                },

                removeHandler: (channel) => ipcMain.removeHandler(channel),
                removeListener: (channel, listener) => ipcMain.removeListener(channel, listener),
            },

            sender: args.webContents,
        })
    }
}
