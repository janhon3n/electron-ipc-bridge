import { IIpcDeclaration, IIpcEventChannel } from "../types"
import { IpcEventChannel } from "../shared/ipc-event-channel"

export interface IpcSender {
    send: (channel: string, ...args: any[]) => void;
}
export interface IpcReceiver {
    on(channel: string, listener: (...args: any[]) => void): void;
    removeListener(channel: string, listener: (...args: any[]) => void): void;
    handle(channel: string, handler: (...args: any[]) => Promise<any> | any): void;
    removeHandler(channel: string): void;
}


export class IpcExposer {

    constructor(protected ipc: {
        declaration: IIpcDeclaration,
        object: any,
        sender: IpcSender,
        receiver: IpcReceiver
    }) { }

    public async setup() {

        console.info('Setting up ' + this.ipc.declaration.name)

        this.ipc.receiver.on(
            'subscribe-' + this.ipc.declaration.name,
            this.subscribeHandler.bind(this)
        )
        this.ipc.receiver.on(
            'unsubscribe-' + this.ipc.declaration.name,
            this.unsubscribeHandler.bind(this)
        )
        this.ipc.receiver.on(
            'next-' + this.ipc.declaration.name,
            this.nextHandler.bind(this)
        )

        this.ipc.receiver.handle(
            'invoke-' + this.ipc.declaration.name,
            this.invokeHandler.bind(this)
        )
    }

    public async cleanup() {

        console.info('Cleaning up ' + this.ipc.declaration.name)

        this.ipc.receiver.removeListener(
            'subscribe-' + this.ipc.declaration.name,
            this.subscribeHandler,
        )
        this.ipc.receiver.removeListener(
            'unsubscribe-' + this.ipc.declaration.name,
            this.unsubscribeHandler,
        )
        this.ipc.receiver.removeListener(
            'next-' + this.ipc.declaration.name,
            this.nextHandler,
        )
        this.ipc.receiver.removeHandler(
            'invoke-' + this.ipc.declaration.name,
        )
    }

    private async invokeHandler(
        functionName: string,
        ...args: any[]
    ) {

        if (!this.ipc.declaration.methods.includes(functionName)) {
            throw Error(`Function ${functionName} has not been exposed`)
        }

        if (typeof (this.ipc.object)[functionName] !== 'function') {
            throw Error(`Function ${functionName} not found on ${this.ipc.declaration.name}`)
        }

        try {
            const data = await (this.ipc.object)[functionName].bind(this)(...args)
            return { data }
        } catch (error) {
            return { error: makeErrorIpcSafe(error) || 'Unknown Error' }
        }
    }

    private subscribeHandler(
        args: {
            subscriptionId: string
            channelName: string
        }
    ) {
        const { channelName, subscriptionId } = args

        const [channel, error] = this.getEventChannel(channelName)
        if (error || !channel) {
            this.ipc.sender.send('subscription-error-' + subscriptionId, error)
            return
        }

        console.info('IpcEventChannel subscribed', { serviceName: this.ipc.declaration.name, channelName, subscriptionId })

        channel.addListener(subscriptionId, (data) => {
            this.ipc.sender.send('subscription-next-' + subscriptionId, data)
        })
    }

    private unsubscribeHandler(
        args: {
            subscriptionId: string
            channelName: string
        }
    ) {
        const { channelName, subscriptionId } = args

        const [channel, error] = this.getEventChannel(channelName)
        if (error || !channel) {
            this.ipc.sender.send('subscription-error-' + subscriptionId, error)
            return
        }
        console.info('IpcEventChannel unsubscribed', { serviceName: this.ipc.declaration.name, channelName, subscriptionId })

        channel.removeListener(subscriptionId)
    }

    private nextHandler(
        args: {
            channelName: string
            value: any
        }
    ) {
        const { channelName, value } = args

        const [channel, error] = this.getEventChannel(channelName)

        if (error || !channel) {
            console.error(error)
            return
        }

        channel.next(value)
    }

    private getEventChannel(name: string): [IIpcEventChannel<any> | null, string | null] {

        if (!this.ipc.declaration.eventChannels.includes(name)) {
            const errorMessage = `IpcEventSource ${name} has not been exposed on service ${this.ipc.declaration.name}`
            console.error(errorMessage)
            return [null, errorMessage]
        }

        const eventSource = (this.ipc.object)[name] as IIpcEventChannel<any>

        if (eventSource == null || !(eventSource instanceof IpcEventChannel)) {
            const errorMessage = `IpcEventSource ${name} not found on service ${this.ipc.declaration.name}`
            console.error(errorMessage)
            return [null, errorMessage]
        }

        return [eventSource, null]
    }
}

export function makeErrorIpcSafe(error: any, depth?: number): any {
    if (!depth) depth = 0

    if (typeof error === 'number' || typeof error === 'boolean') {
        return error
    } else if (typeof error === 'string') {
        return error.substr(0, 500)
    } else if (typeof error === 'object' && error !== null && depth <= 3) {
        const cleanedError: { [x: string]: any } = {}
        for (const key of Object.getOwnPropertyNames(error)) {
            const value = error[key]
            const cleanedValue = makeErrorIpcSafe(value, depth + 1)
            if (cleanedValue !== undefined) cleanedError[key] = cleanedValue
        }
        return cleanedError
    } else if (Array.isArray(error)) {
        return error.map((i) => {
            return makeErrorIpcSafe(i, depth)
        })
    }

    return undefined
}
