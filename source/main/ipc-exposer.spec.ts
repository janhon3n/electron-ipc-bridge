import { IpcExposer } from './ipc-exposer'
import { IpcReceiver, IpcSender, makeErrorIpcSafe } from './ipc-exposer'
import { IpcValue } from '../shared/ipc-value'
import { IpcEventChannel } from '../shared/ipc-event-channel'

describe('ipc-exposer', () => {

    let receiver: IpcReceiver
    let sender: IpcSender

    let listeners: {
        [key: string]: ((...args: any[]) => void)
    }

    let handlers: {
        [key: string]: ((...args: any[]) => Promise<any>)
    }

    beforeEach(() => {

        listeners = {}
        handlers = {}

        receiver = {
            on: (channel, listener) => listeners[channel] = listener,
            removeListener: (channel) => delete listeners[channel],
            handle: (channel, handler) => handlers[channel] = handler,
            removeHandler: (channel) => delete handlers[channel],
        }

        sender = {
            send: jest.fn()
        }
    })


    it("exposes object's methods and eventChannels declared in the declaration to the ipc bridge", async () => {

        const object = {
            eventSource1: new IpcEventChannel(),
            value1: new IpcValue(1),
            method1: async () => {
                await new Promise(resolve => setTimeout(resolve, 100))
                console.info('method1')
            }
        }
        const exposer = new IpcExposer({
            declaration: {
                name: 'test',
                methods: ['method1'],
                eventChannels: ['eventSource1', 'value1'],
            },
            receiver,
            sender,
            object,
        })

        await exposer.setup()

        expect(Object.keys(handlers)).toHaveLength(1)

        // Expect handler for invoking class methods
        expect(handlers).toHaveProperty('invoke-test')

        expect(Object.keys(listeners)).toHaveLength(3)
        // Expect listeners for eventChannel subscriptions
        expect(listeners).toHaveProperty('subscribe-test')
        expect(listeners).toHaveProperty('unsubscribe-test')
        // Expect listener for eventChannels next value
        expect(listeners).toHaveProperty('next-test')

        // Cleans them up
        await exposer.cleanup()
        expect(Object.keys(listeners)).toHaveLength(0)
        expect(Object.keys(handlers)).toHaveLength(0)
    })



    it("class's IpcEventChannels can be subscribed to, and after that every next value is transmitted", async () => {

        const object = {
            channel1: new IpcEventChannel(),
            value1: new IpcValue(1),
            method1: async () => {
                await new Promise(resolve => setTimeout(resolve, 100))
                console.info('method1')
            }
        }
        const exposer = new IpcExposer({
            declaration: {
                name: 'test',
                methods: ['method1'],
                eventChannels: ['channel1', 'value1'],
            },
            receiver,
            sender,
            object,
        })

        await exposer.setup()

        listeners['subscribe-test']({
            subscriptionId: 'asdf',
            channelName: 'value1',
        })

        const sentMessages = sender.send as jest.Mock

        // IpcValue sends current value immediately
        expect(sentMessages).toHaveBeenCalledTimes(1)
        expect(sentMessages.mock.calls[0]).toEqual(['subscription-next-asdf', 1])

        // when IpcValue's value changes, sends the next value
        object.value1.next(2)
        expect(sentMessages).toHaveBeenCalledTimes(2)
        expect(sentMessages.mock.calls[1]).toEqual(['subscription-next-asdf', 2])
        object.value1.next(5)
        expect(sentMessages).toHaveBeenCalledTimes(3)
        expect(sentMessages.mock.calls[2]).toEqual(['subscription-next-asdf', 5])

        // when unsubscribed, stops sending values
        listeners['unsubscribe-test']({
            subscriptionId: 'asdf',
            channelName: 'value1',
        })
        object.value1.next(100)
        expect(sentMessages).toHaveBeenCalledTimes(3) // still only 3 calls

        await exposer.cleanup()

    })

    it('handles multiple distinct subscriptions to the same IpcEventChannel', async () => { })

    it("IpcEventChannel's next value can also be set over the IPC", async () => {

        const object = {
            channel1: new IpcEventChannel(),
            value1: new IpcValue(1),
            method1: async () => {
                await new Promise(resolve => setTimeout(resolve, 100))
                console.info('method1')
            }
        }
        const exposer = new IpcExposer({
            declaration: {
                name: 'test',
                methods: ['method1'],
                eventChannels: ['channel1', 'value1'],
            },
            receiver,
            sender,
            object,
        })

        await exposer.setup()

        await listeners['next-test']({
            channelName: 'value1',
            value: 2,
        })

        expect(object.value1.value).toEqual(2)

    })

    it("class's public methods can be called over the IPC", async () => {

        const object = {
            eventSource1: new IpcEventChannel(),
            value1: new IpcValue(1),
            method1: async (arg: any) => {
                console.info('method1 called')
                await new Promise(resolve => setTimeout(resolve, 100))

                if (arg === 'fail') {
                    throw 'failed'
                }

                return 'success'
            }
        }
        const exposer = new IpcExposer({
            declaration: {
                name: 'test',
                methods: ['method1'],
                eventChannels: ['eventSource1', 'value1'],
            },
            receiver,
            sender,
            object,
        })

        await exposer.setup()


        const res = await handlers['invoke-test'](
            'method1', 1, 2, 3, 4
        )
        expect(res.data).toEqual('success')
        expect(res.error).toEqual(undefined)


        const res2 = await handlers['invoke-test'](
            'method1', 'fail'
        )
        expect(res2.data).toEqual(undefined)
        expect(res2.error).toEqual('failed')

        await exposer.cleanup()
    })

    test('makeErrorIpcSafe', () => {
        const error = new Error('Testi virhe') as any
        error.extraDetail = 'Extra detailia'
        error.extraFunction = () => { }
        error.subError = new Error()

        const safeError = makeErrorIpcSafe(error)
        console.info(safeError)
    })
})
