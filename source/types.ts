export type IIpcMethod = () => any | Promise<any>

export interface IIpcEventSource<E> {
    addListener: (listenerId: string, listener: (event: E) => void) => void
    removeListener: (listenerId: string) => void
}

export interface IIpcEventEmitter<E> {
    next: (event: E) => void
}

export interface IIpcEventChannel<E> extends IIpcEventEmitter<E>, IIpcEventSource<E> { }

export interface IIpcValue<V> extends IIpcEventSource<V>, IIpcEventEmitter<V> {
    value: V
}

export interface IIpcDeclaration {
    name: string
    methods: string[]
    eventChannels: string[]
}
