import { useState, useEffect } from 'react'
import { ElectronIpcBridgeAPI } from '../preload/bridge-api-type'

export function ipcCall<D = any>(
    target: {
        serviceName: string
        functionName: string
    },
    ...args: any[]
): Promise<D> {
    return ((window as any).electronIpcBridgeAPI as ElectronIpcBridgeAPI).invokeMethod(target, ...args)
}

interface Props {
    serviceName: string
    functionName: string
}

export function useIpcCall<D = any>(props: Props) {
    const { functionName, serviceName } = props

    const [called, setCalled] = useState<boolean>(true)
    const [loading, setLoading] = useState<boolean>(true)
    const [error, setError] = useState<Error>()
    const [data, setData] = useState<D>()

    useEffect(() => {
        const call = async (...args: any) => {
            setCalled(true)
            setLoading(true)
            try {
                const data = await ipcCall<D>(
                    { serviceName, functionName },
                    ...args
                )
                setData(data)
                setError(undefined)
                setLoading(false)
                return data
            } catch (err) {
                setError(err as any)
                setLoading(false)
            }
        }

        call()
    }, [])

    return {
        called,
        loading,
        error,
        data,
    }
}

export function useIpcCallLazy<D = any>(props: Props) {
    const { functionName, serviceName } = props

    const [called, setCalled] = useState<boolean>(false)
    const [loading, setLoading] = useState<boolean>(false)
    const [error, setError] = useState<Error>()
    const [data, setData] = useState<D | undefined>()

    return {
        called,
        loading,
        error,
        data,
        call: async (...args: any) => {
            setCalled(true)
            setLoading(true)
            try {
                const data = await ipcCall<D>(
                    { serviceName, functionName },
                    ...args
                )
                setData(data)
                setError(undefined)
                setLoading(false)
                return data
            } catch (err) {
                setError(err as any)
                setLoading(false)
                throw err
            }
        },
    }
}
