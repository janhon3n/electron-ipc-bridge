import { deleteTypenames } from './apollo-link'

describe('ApolloLink', () => {
    test('Deletes __typename fields from variables, so you can reuse queried data as variables without breaking things', () => {
        let variables: any = {
            __typename: 'Client',
            product: { __typename: 'Product' },
            info: { a: 'b' },
        }

        variables = deleteTypenames(variables)
        expect('__typename' in variables).toEqual(false)
        expect('__typename' in variables.product).toEqual(false)
        expect('__typename' in variables.info).toEqual(false)
        expect(variables.info.a).toEqual('b')
    })
})
