import { useEffect } from 'react'
import { IIpcEventChannel } from '..'
import { IpcEventChannel } from '../shared/ipc-event-channel'

interface Props<D> {
    channel: IIpcEventChannel<D>
    callback: (value: D) => void
}

export function useIpcEventEmitter<D = any>(props: Props<D>) {
    const { channel, callback } = props

    useEffect(() => {
        const listenerId = Math.random().toString()

        const listener = (data: D) => {
            callback(data)
        }
        channel.addListener(listenerId, listener)
        return () => channel.removeListener(listenerId)
    }, [])
}
