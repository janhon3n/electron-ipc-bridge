import { ApolloLink, Observable } from '@apollo/client'
import { ipcCall } from './ipc-call'

export function createIpcApolloLink(options: {
    serviceName: string
    operationNames: string[]
    log?: boolean
}): ApolloLink {
    const { serviceName, operationNames, log } = options
    return new ApolloLink((operation, forward) => {
        if (operationNames.includes(operation.operationName)) {
            const variables = deleteTypenames(operation.variables)
            return new Observable((subscriber) => {
                ipcCall(
                    {
                        serviceName,
                        functionName: operation.operationName,
                    },
                    variables
                )
                    .then((data) => {
                        if (!data) throw Error('Missing data')

                        const baseTypename =
                            operation.operationName.substr(0, 1).toUpperCase() +
                            operation.operationName.substr(1)
                        fillMissingTypenames(baseTypename, data)

                        if (log) {
                            console.info(
                                'Operation ' + operation.operationName + ' succesful',
                                { variables, data }
                            )
                        }

                        subscriber.next({ data })
                        subscriber.complete()
                    })
                    .catch((error) => {
                        if (log) {
                            console.warn(
                                'Operation ' + operation.operationName + ' error',
                                error
                            )
                        }
                        subscriber.error(error)
                    })
            })
        } else {
            return forward(operation)
        }
    })
}

export function deleteTypenames(object: any) {
    const variables = {}
    for (const [key, value] of Object.entries(object)) {
        if (key !== '__typename') {
            if (typeof value === 'object' && value != null) {
                //@ts-ignore
                variables[key] = deleteTypenames(value)
            } else {
                //@ts-ignore
                variables[key] = value
            }
        }
    }
    return variables
}

function _fillMissingTypenames(nameSoFar: string, object: any) {
    /* Fills missing typenames recursively for a object tree so that graphql likes it better */
    if (!object.__typename) object.__typename = nameSoFar
    for (const key of Object.keys(object)) {
        const capitalized = key.charAt(0).toUpperCase() + key.substring(1)
        const value = object[key]
        if (Array.isArray(value)) {
            for (const item of value) {
                fillMissingTypenames(nameSoFar + capitalized, item)
            }
        } else if (typeof value === 'object' && value !== null) {
            fillMissingTypenames(nameSoFar + capitalized, value)
        }
    }
}

function fillMissingTypenames(baseName: string, object: any) {
    const capitalized = baseName.charAt(0).toUpperCase() + baseName.substring(1)
    if (Array.isArray(object)) {
        for (const item of object) {
            _fillMissingTypenames(capitalized, item)
        }
    } else {
        _fillMissingTypenames(capitalized, object)
    }
}
