import { useEffect, useState } from "react"
import { IIpcEventChannel } from ".."

type Output<D> = [value: D | undefined, setValue: (value: D) => void]

export function useIpcValue<D = any>(ipcValue: IIpcEventChannel<D>): Output<D> {
    const [value, setValue] = useState<D | undefined>(undefined)

    useEffect(() => {
        const listenerId = Math.random().toString()
        const listener = (data: D) => setValue(data)
        ipcValue.addListener(listenerId, listener)
        return () => ipcValue.removeListener(listenerId)
    }, [])

    return [value, (value: D) => ipcValue.next(value)]
}
