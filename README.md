# electron-ipc-bridge

Helper library for electron inter process communication.

Can be used to expose objects with methods and eventEmitters from the main process to the renderer processes. The exposed objects can be used from the renderer processes via stubs with the same interface as the exposed object.

Contains React hooks for calling the exposed methods or subscribe to the exposed eventEmitters.


### Tips

To avoid build and dependency issues, import directly from the folder you need
```
// from electron project
import { ElectronIpcService } from 'electron-ipc-bridge/dist/main'
import { IpcEventEmitter, IpcValue } from 'electron-ipc-bridge/dist/shared'

// from react project
import { useIpcValue, useIpcCall } from 'electron-ipc-bridge/dist/react'
```

